﻿using System.Windows;
using System.Windows.Documents;

namespace ESpritePacker
{
    /// <summary>
    /// Interaction logic for About.xaml
    /// </summary>
    public partial class About : Window
    {
        public About() {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e) {
            Close();
        }

        private void Hyperlink_Click(object sender, RoutedEventArgs e) {
            var url = (sender as Hyperlink)?.NavigateUri;
            if (url != null)
                System.Diagnostics.Process.Start(url.OriginalString);
        }
    }
}
